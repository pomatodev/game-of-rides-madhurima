package com.example.madhurima.gameofthrones;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.SeekBar;


public class EnquiryPage extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry_page);

        //Spinner 1
        Spinner spinner = (Spinner) findViewById(R.id.kingdom_spinner);
            // Create an ArrayAdapter using the string array and a default spinner          layout
        ArrayAdapter<CharSequence> adapter =   ArrayAdapter.createFromResource(this,
                R.array.kingdom_arrays, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        //Spinner 2
        Spinner spinner1 = (Spinner) findViewById(R.id.brand_spinner);
        // Create an ArrayAdapter using the string array and a default spinner          layout
        ArrayAdapter<CharSequence> adapter1 =   ArrayAdapter.createFromResource(this,
                R.array.brand_arrays, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner1.setAdapter(adapter1);

        //Spinner 3
        Spinner spinner2 = (Spinner) findViewById(R.id.model_spinner);
        // Create an ArrayAdapter using the string array and a default spinner          layout
        ArrayAdapter<CharSequence> adapter2 =   ArrayAdapter.createFromResource(this,
                R.array.model_arrays, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner2.setAdapter(adapter2);


        //SeekBar 1
        ///SeekBar seekBar;
        ///TextView textView;
        ///super.onCreate(savedInstanceState);
        ///setContentView(R.layout.activity_enquiry_page);
        ///seekBar = (SeekBar) findViewById(R.id.seekBarMin);
        ///textView = (TextView) findViewById(R.id.textView4);
        // Initialize the textview with '0'
        ///textView.setText(seekBar.getProgress() + "/" + seekBar.getMax());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu;
        //this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_enquiry_page, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
